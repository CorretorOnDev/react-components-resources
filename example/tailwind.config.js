const path = require('path')
require('dotenv').config()

module.exports = {
  purge:{
    content: [
      './pages/**/*.{js,ts,jsx,tsx}',
      './components/**/*.{js,ts,jsx,tsx}'
    ],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
    },

  },
  variants: {
    extend: {},
  },
  plugins: [],
  corePlugins: {
    backgroundImage: true,
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles/scss')],
  },
}
