import React, {useState} from 'react'

const Produto = ({title = ''}) => {
  const [texto, setTexto] = useState('');
  return (
    <div>
      <h1>Produto: {title}</h1>
      <input type="text" value={texto} onChange={e => setTexto(e.target.value)}/>
    </div>
  )
}

export default Produto
