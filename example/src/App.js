import React, {useState} from 'react'
import {Dock, DockWindow} from 'react-components-resources'
import {v4 as uuidV4} from 'uuid'
import _ from 'lodash'
import {Button} from "primereact/button";

const App = () => {
  const [emptyDockWindow, setEmptyDockWindow] = useState([])
  const [qtdDockWindow, setQtdDockWindow] = useState(0)

  const dockChange = (dispatch, value) => {
    dispatch(oldValue =>
      _.map(oldValue, (data) => {
        if (data.i === value.i) {
          return { ...data, ...value }
        }
        return data
      })
    )
  }

  const dockRemove = (dispatch, value) => {
    dispatch(oldValue =>
      _.remove(oldValue, (data) => {
        return data.i !== value
      })
    )
  }

  const openEmptyDock = () => {
    setEmptyDockWindow(oldProdutos => [...oldProdutos, ...[{
      i: uuidV4(),
      title: `Window - ${qtdDockWindow}`,
      isDialog: false
    }]])
    setQtdDockWindow((oldQtd) => ++oldQtd)
  }


  return (
    <div>
      <div className={'flex flex-wrap'}>
        <Button className={'p-button-sm mr-1'} id={'btn-add-empty-dock-window'} onClick={openEmptyDock}>Add empty DockWindow</Button>
      </div>
      <Dock>

        {emptyDockWindow.map(data =>
          <DockWindow key={data.i} {...data}
                      onDragStop={(value) => dockChange(setEmptyDockWindow, value)}
                      setIsDialog={(value) => dockChange(setEmptyDockWindow, value)}
                      onClose={(i) => dockRemove(setEmptyDockWindow, i)}
                      onDialogFullScreen={(value) => dockChange(setEmptyDockWindow, value)}
          >

          </DockWindow>
        )}

        <DockWindow
          key={'fixed-window'}
          title={'Fixed window'}
          isDialog={false}
          isDraggable={false}
          isResizable={false}
        >
          Fixed window
        </DockWindow>

      </Dock>
    </div>
  )
}

export default App
