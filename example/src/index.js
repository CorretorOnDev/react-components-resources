import './styles/app.css'
import 'react-components-resources/src/styles/scss/primereact/primereact-custom-theme.scss'
import "react-components-resources/src/styles/scss/dock.scss"


import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

ReactDOM.render(
    <App />
  , document.getElementById('root'))
