# react-components-resources

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/react-components-resources.svg)](https://www.npmjs.com/package/react-components-resources) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-components-resources
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'react-components-resources'
import 'react-components-resources/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
