const initialValue = {
  i: null,
  x: 0,
  y: 0,
  w: 2,
  h: 8,
  minW: 1,
  minH: 4,
  resizeHandles: ['se', 's', 'e'],
  title: null,
  isDialog: true,
  dialog_x: 0,
  dialog_y: 0,
  dialog_w: '50vw',
  dialog_h: '30vw',
  fullScreen: false,
  isDraggable: true,
  isFullScreen: false,
  zIndex: 0
}

export default initialValue
