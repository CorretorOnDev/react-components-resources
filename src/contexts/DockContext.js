import React, { createContext } from 'react'
import _ from 'lodash'

const DockContext = createContext()

const DockProvider = ({ children, components = {} }) => {


  return (
    <DockContext.Provider
      value={{

      }}
    >
      {children}
    </DockContext.Provider>
  )
}

export { DockProvider }

export default DockContext
