import './styles/scss/dock.scss'
import './styles/scss/primereact/primereact-custom-theme.scss'

/* Components -------------- */
export { default as Dock } from './components/dock/Dock'
export { default as DockWindow } from './components/dock/DockWindow'
export { default as Panel } from './components/Panel'
export { default as Dialog } from './components/Dialog'

/* Libs -------------- */
export {
  dockMapDispatchToProps,
  newDockWindow,
  updateDockWindow,
  removeDockWindow
} from './libs/dock'
