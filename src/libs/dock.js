import _ from 'lodash'
import { v4 as uuidV4 } from 'uuid'

/**
 * Mapeia os dispatchs para incluir a propriedade i do dock no action usado no reducers
 * @param mapDispatchToProps
 * @returns {function(*, *=): *}
 */
export const dockMapDispatchToProps =
  (mapDispatchToProps) => (dispatch, props) => {
    return mapDispatchToProps(
      (callAction) =>
        callAction((action) => {
          dispatch({ ...action, ...{ i: props.i } })
        }),
      props
    )
  }

/**
 * Adiciona um novo Dock Window
 * @param state
 * @param payload
 * @returns {*[]}
 */
export const newDockWindow = (state = [], payload = {}) => {
  return [
    ...state,
    ...[
      {
        ...{
          i: payload.i ? payload.i : uuidV4(),
          zIndex: payload.zIndex ? payload.zIndex : state.length + 1
        },
        ...payload
      }
    ]
  ]
}

/**
 * Atualiza um Dock Window
 * @param state
 * @param payload
 * @param i
 * @returns {*[]}
 */
export const updateDockWindow = (state = [], payload = {}, i) => {
  return _.map(state, (data) => {
    if (data.i === i) {
      return { ...data, ...payload }
    }
    return data
  })
}

/**
 * Remove um Dock window
 * @param state
 * @param i
 * @returns {*[]}
 */
export const removeDockWindow = (state, i) => {
  return _.remove(state, (data) => {
    return data.i !== i
  })
}
