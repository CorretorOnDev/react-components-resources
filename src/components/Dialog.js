import React, { useRef } from 'react'
import Draggable from 'react-draggable'
import {
  faWindowClose,
  faWindowMaximize,
  faWindowMinimize,
  faWindowRestore
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from 'primereact/button'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import Panel from './Panel'

const defaultStyles = {
  minWidth: '339px',
  minHeight: '150px'
}

const Dialog = (props) => {
  const nodeRef = useRef(null)
  const fullScreenStyles = () => {
    return props.isFullScreen
      ? {
          width: '100vw',
          height: '100vh',
          top: 0,
          left: 0
        }
      : {}
  }

  const fullScreenPositions = () => {
    return props.isFullScreen ? { x: 0, y: 0 } : {}
  }

  return (
    <Draggable
      handle='.p-panel-header'
      cancel='.header-buttons'
      defaultPosition={{ x: 10, y: 100 }}
      position={{
        ...{ x: props.dialog_x, y: props.dialog_y },
        ...fullScreenPositions(props.isFullScreen)
      }}
      // positionOffset={{x: '20%', y: '50%'}}
      disabled={props.isFullScreen || !props.isDraggable}
      // grid={[2, 2]}
      // scale={1}
      onMouseDown={props.onMouseDown}
      onStart={props.onDragStart}
      onDrag={props.onDrag}
      onStop={props.onDragStop}
      nodeRef={nodeRef}
    >
      <div
        className='fixed resize overflow-hidden flex shadow-2xl border border-white'
        ref={nodeRef}
        style={{
          ...defaultStyles,
          ...{
            zIndex: props.zIndex,
            width: props.dialog_w,
            height: props.dialog_h
          },
          ...fullScreenStyles(props.isFullScreen)
        }}
      >
        <Panel
          style={props.style}
          className='shadow-md w-full'
          headerClassName={classNames({ 'cursor-move': !props.isFullScreen && props.isDraggable })}
          {...props}
          key={`panel-${props.i}`}
          headerButtons={
            <React.Fragment>
              <Button
                type='button'
                className='px-1 border-0 rounded'
                title='Minimizar'
                onClick={props.onMinimize}
              >
                <FontAwesomeIcon icon={faWindowMinimize} />
              </Button>
              <Button
                type='button'
                className='px-1 border-0 rounded'
                title={props.isFullScreen ? 'Restaurar' : 'Maximizar'}
                onClick={() => props.onFullScreen(!props.isFullScreen)}
              >
                {props.isFullScreen ? (
                  <FontAwesomeIcon icon={faWindowRestore} />
                ) : (
                  <FontAwesomeIcon icon={faWindowMaximize} />
                )}
              </Button>
              <Button
                type='button'
                className='px-1 border-0 rounded'
                title='Fechar'
                onClick={props.onClose}
              >
                <FontAwesomeIcon icon={faWindowClose} />
              </Button>
            </React.Fragment>
          }
        >
          {props.children}
        </Panel>
      </div>
    </Draggable>
  )
}

Dialog.propTypes = {
  title: PropTypes.string,
  isFullScreen: PropTypes.bool,
  isDraggable: PropTypes.bool,
  onMinimize: PropTypes.func,
  onFullScreen: PropTypes.func,
  onClose: PropTypes.func,
  onDragStart: PropTypes.func,
  onDrag: PropTypes.func,
  onDragStop: PropTypes.func,
  onMouseDown: PropTypes.func
}

Dialog.defaultProps = {
  title: '',
  isFullScreen: false,
  isDraggable: true,
  onMinimize: () => {},
  onFullScreen: () => {},
  onClose: () => {},
  onDragStart: () => {},
  onDrag: () => {},
  onDragStop: () => {},
  onMouseDown: () => {}
}

export default Dialog
