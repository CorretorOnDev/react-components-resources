import React from 'react'
import { Panel } from 'primereact/panel'
import PropTypes from 'prop-types'

const headerTemplate = (options) => {
  const className = `${options.className} ${options.headerClassName} py-1 flex justify-between`
  const titleClassName = `${options.titleClassName} overflow-ellipsis overflow-hidden whitespace-nowrap`

  return (
    <div className={className}>
      <div className={titleClassName} title={options.title}>
        {options.title}
      </div>
      <span className='flex header-buttons'>{options.headerButtons}</span>
    </div>
  )
}

const CustomPanel = (props) => {
  return (
    <Panel
      {...props}
      headerTemplate={(options) =>
        headerTemplate({
          ...options,
          ...{
            headerButtons: props.headerButtons,
            title: props.title,
            headerClassName: props.headerClassName
          }
        })
      }
    >
      {props.children}
    </Panel>
  )
}

CustomPanel.propTypes = {
  children: PropTypes.any,
  headerButtons: PropTypes.any,
  headerClassName: PropTypes.string
}

export default CustomPanel
