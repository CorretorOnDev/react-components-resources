import React, { useState } from 'react'
import GridLayout, { WidthProvider } from 'react-grid-layout'
import classNames from 'classnames'
import dockWindowInitialValue from '../../data/dockWindow'
import _ from 'lodash'

const ReactGridLayout = WidthProvider(GridLayout)

const Dock = ({ children }) => {
  const [isDragging, setIsDragging] = useState(false)

  const components = React.Children.map(children, (child) => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child)
    }
    return child
  })

  // const resetZindex = (i) => {
  //   const oldZIndex = _.orderBy(components, (el) => el.props.zIndex, 'asc')
  //
  //   if (_.get(_.last(oldZIndex), 'props.i') === i) {
  //     return null
  //   }
  //   let index = 0
  //   return _.map(oldZIndex, (item) => {
  //     let zIndex = index
  //     if (item.i === i) {
  //       zIndex = oldZIndex.length - 1
  //     } else {
  //       ++index
  //     }
  //     oldZIndex.props.onDialogMouseDown()
  //     return { ...item, ...{ zIndex: zIndex } }
  //   })
  // }

  const change = (data) => {
    // resetZindex()
    // console.log(data, components)
    // _.forEach(data, (value) => {
    //   console.log(value.i)
    // })
  }

  return (
    <React.Fragment>
      <div
        className={classNames('absolute z-10', { 'opacity-10': isDragging })}
      >
        {components.filter((child) => child.props.isDialog)}
      </div>

      <ReactGridLayout
        className='layout'
        cols={4}
        rowHeight={30}
        width='100%'
        draggableHandle='.p-panel-header'
        onLayoutChange={(value) => change(value)}
        onDrag={() => setIsDragging(true)}
        onDragStop={() => setIsDragging(false)}
      >
        {components
          .filter((child) => !child.props.isDialog)
          .map((child) => (
            <div
              key={child.props.i}
              data-grid={{ ...dockWindowInitialValue, ...child.props }}
              className='flex flex-col bg-white'
            >
              {child}
            </div>
          ))}
      </ReactGridLayout>
    </React.Fragment>
  )
}

export default Dock
