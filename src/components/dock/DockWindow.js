import React, { useState } from 'react'
import Dialog from '../Dialog'
import PropTypes from 'prop-types'
import Panel from '../Panel'
import { faExternalLinkAlt, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import _ from 'lodash'
import dockWindowInitialValue from '../../data/dockWindow'
import classNames from "classnames";

const DockWindow = (props) => {
  const [isDragging, setIsDragging] = useState(false)
  const [dialogX, setDialogX] = useState(0)
  const [dialogY, setDialogY] = useState(0)

  const childrenWithDockData = React.Children.map(props.children, (child) => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child)
    }
    return child
  })

  const draggingDialog = _.debounce((i, event, data) => {
    setDialogX(data.x)
    setDialogY(data.y)
    props.onDrag({ dialog_x: data.x, dialog_y: data.y, i }, event)
  }, 10)

  const onDragStart = (i, event, data) => {
    setDialogX(data.x)
    setDialogY(data.y)
    setIsDragging(true)
    props.onDragStart({ dialog_x: data.x, dialog_y: data.y, i }, event)
  }
  const onDragStop = (i, event, data) => {
    props.onDragStop({ dialog_x: data.x, dialog_y: data.y, i }, event)
    setIsDragging(false)
  }

  const onDialogMouseDown = async (i, e) => {
    // let index = 0
    // setDock(
    //   (() => {
    //     const _oldLayout = _.orderBy(dock, ['zIndex'], ['asc'])
    //
    //     if (_.get(_.last(_oldLayout), 'i') === i) {
    //       return dock
    //     }
    //
    //     return _.map(_oldLayout, (item) => {
    //       let zIndex = index
    //       if (item.i === i) {
    //         zIndex = _oldLayout.length - 1
    //       } else {
    //         ++index
    //       }
    //       return { ...item, ...{ zIndex: zIndex } }
    //     })
    //   })()
    // )
  }

  if (props.isDialog) {
    return (
      <Dialog
        key={`dialog-${props.i}`}
        {...props}
        dialog_x={isDragging ? dialogX : props.dialog_x}
        dialog_y={dialogY}
        style={{}}
        onMinimize={() => props.setIsDialog({ i: props.i, isDialog: false })}
        onClose={() => props.onClose(props.i)}
        onDrag={(e, _data) => draggingDialog(props.i, e, _data)}
        onDragStart={(e, _data) => onDragStart(props.i, e, _data)}
        onDragStop={(e, _data) => onDragStop(props.i, e, _data)}
        onMouseDown={(e) => onDialogMouseDown(props.i, e)}
        onFullScreen={(isFullScreen) =>
          props.onDialogFullScreen({ i: props.i, isFullScreen })
        }
      >
        {childrenWithDockData}
      </Dialog>
    )
  } else {
    return (
      <Panel
        {...props}
        className='h-full'
        headerClassName={classNames({ 'cursor-move': props.isDraggable })}
        headerButtons={
          <React.Fragment>
            <button
              type='button'
              className='text-xs p-1'
              onClick={() => props.setIsDialog({ i: props.i, isDialog: true })}
            >
              <FontAwesomeIcon icon={faExternalLinkAlt} />
            </button>
            <button
              type='button'
              className='text-xs p-1'
              onClick={() => props.onClose(props.i)}
            >
              <FontAwesomeIcon icon={faTimes} />
            </button>
          </React.Fragment>
        }
      >
        {childrenWithDockData}
      </Panel>
    )
  }
}

Dialog.propTypes = {
  title: PropTypes.string,
  isDialog: PropTypes.bool.isRequired,
  isFullScreen: PropTypes.bool,
  isDraggable: PropTypes.bool,
  onMinimize: PropTypes.func,
  onDragStart: PropTypes.func,
  onDrag: PropTypes.func,
  onDragStop: PropTypes.func,
  onClose: PropTypes.func,
  onMouseDown: PropTypes.func,
  onFullScreen: PropTypes.func,
  setIsDialog: PropTypes.func,
  onDialogFullScreen: PropTypes.func
}

DockWindow.defaultProps = {
  ...dockWindowInitialValue,
  onMinimize: () => {},
  onDragStart: () => {},
  onDrag: () => {},
  onDragStop: () => {},
  onClose: () => {},
  onMouseDown: () => {},
  onFullScreen: () => {},
  setIsDialog: () => {},
  onDialogFullScreen: () => {}
}

export default DockWindow
