import React from 'react'
import { InputMask } from 'primereact/inputmask'
import classNames from 'classnames'

const InputTelefone = ({
  className = null,
  onChange,
  placeholder = '(xx) xxxxx-xxxx',
  value
}) => {
  return (
    <InputMask
      className={classNames(className)}
      type='tel'
      mask='(99) 99999-9999'
      placeholder={placeholder}
      value={value}
      onChange={onChange}
    />
  )
}

export default InputTelefone
