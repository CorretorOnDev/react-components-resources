describe('DockWindow', () => {

  beforeEach(() => {
    cy.visit('/')

    cy.get('#btn-add-empty-dock-window').click()
    cy.get('.react-grid-layout > .react-grid-item').as('dockWindow')
      .should('have.length', 2)
  })

  const openDockWindow = (n) => {
    for (var i = 0; i < n; i++) {
      cy.get('#btn-add-empty-dock-window').click()
    }
  }

  it('DockWindow - add multiple', () => {
    openDockWindow(3)

    cy.get('.react-grid-item')
      .should('have.length', 5)
  })

  it('DockWindow - open dialog', () => {
    cy.get('@dockWindow').first().find('.header-buttons>[type="button"]').eq(0).click()
  })

  it('DockWindow - close', () => {
    cy.get('@dockWindow').first().find('.header-buttons>[type="button"]').eq(1).click()
    cy.get('.react-grid-item').should('have.length', 1)
  })

  it('DockWindow - resize', () => {
    cy.get('@dockWindow').first()
      .find('.react-resizable-handle-se')
      .trigger('mousedown')
      .trigger('mousemove', { clientX: 700, clientY: 500 })
      .trigger('mouseup', {force: true})

    cy.get('@dockWindow').first()
      .find('.react-resizable-handle-se')
      .trigger('mousedown')
      .trigger('mousemove', { clientX: 100, clientY: 100 })
      .trigger('mouseup', {force: true})

    cy.get('@dockWindow').first()
      .find('.react-resizable-handle-se')
      .trigger('mousedown')
      .trigger('mousemove', { clientX: 300, clientY: 500 })
      .trigger('mouseup', {force: true})
  })

  it('DockWindow - reposition', () => {
    openDockWindow(3)

    cy.get('@dockWindow').first()
      .find('.p-panel-header')
      .trigger('mousedown')
      .trigger('mousemove', { clientX: 900})
      .trigger('mouseup', {force: true})
  })


})
